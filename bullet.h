class bullet
{
private:
	double x, y, angle;
public:
	~bullet(void);
    bullet(double x, double y, double angle);

	static GLuint bulletTextureID;
	
	void initialise();	
	void render();
	void update(double deltaT, double prevDeltaT);	
	double getY();
	double getX();
};