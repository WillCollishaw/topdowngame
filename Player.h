class Player
{
private:
	int lives, health, score;
public:
	Player(int lives, int health, int score);

	int getLives();
	void setLives(int setAmount);
	int getHealth();
	void setHealth(int setAmount);
	bool hitMine(int damage);
	int getScore();
	void setScore(int setAmount);
	bool shot(int damage);
	void healthPack();
};