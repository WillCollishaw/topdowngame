#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "SOIL.h"
#include "Wall.h"

Wall::Wall(double x, double y, GLuint inputWall)
{
	this->x = x;
	this->y = y;
	this->wall = inputWall;
}

Wall::~Wall(void)
{
	glDeleteTextures(1, &wall);
}

Wall::Wall(const Wall &m)
{
	this->x = m.x;
	this->y = m.y;
	this->wall = m.wall;
}

void Wall::render()
{
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, wall);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(1, 0);
	glVertex2f(x+4, y);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+4);


	glTexCoord2f(1, 0);
	glVertex2f(x+4, y);

	glTexCoord2f(1, 1);
	glVertex2f(x+4, y+4);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+4);
	glEnd();


	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

double Wall::getX()
{
	return x;
}
double Wall::getY()
{
	return y;
}