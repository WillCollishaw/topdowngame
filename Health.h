class Health
{
private:
	double x,y;

public:
	~Health(void);
	Health(double x, double y);
	Health(const Health &m);

	void initialise();	
	void render();
	double getY();
	double getX();

	static GLuint healthTextureID;
};

