#pragma once
#include <windows.h>		// Header File For Windows
#include <gl/gl.h>			// Header File For The OpenGL32 Library
#include <gl/glu.h>			// Header File For The GLu32 Library
#include "SOIL.h"

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls

#include "DifficultyScreenActivity.h"
#include "GameActivity.h"

DifficultyScreenActivity::DifficultyScreenActivity(OpenGLApplication *app)
	: Activity(app)
{
}

void DifficultyScreenActivity::initialise()
{
	// Load the start screen image as a texture using the SOIL library
	textureID = SOIL_load_OGL_texture("difficulty.png",			// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
	texture2ID = SOIL_load_OGL_texture("helpScreen.png",			// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
	difficultySelected = false;
}


void DifficultyScreenActivity::shutdown()
{
	// Delete the texture
	glDeleteTextures(1, &textureID);
	glDeleteTextures(1, &texture2ID);
}

void DifficultyScreenActivity::onSwitchIn()
{
	// Activity switched in

	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to black
}

void DifficultyScreenActivity::onReshape(int width, int height)
{
	camHalfHeight = height/2;
	camBottomQ = height/4;
	camTopQ = (height/4)+(height/2);

	// Screen resized
	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	gluOrtho2D(-aspect, aspect, -1.0, 1.0);				// set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void DifficultyScreenActivity::render()
{
	//If difficulty has not been selected,  first screen is rendered
	if (!difficultySelected){
		// Clear color buffer
		glClear(GL_COLOR_BUFFER_BIT);

		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, textureID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
		glTexCoord2f(0, 0);
		glVertex2f(-1, -1);

		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);


		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(1, 1);
		glVertex2f(1, 1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);
		glEnd();

		// Disable 2D texturing
		glDisable(GL_TEXTURE_2D);

		glFlush();
	}
	//Once difficulty is selected, second screen is rendered
	else
	{
		// OpenGL render calls go in this method

		// Clear color buffer
		glClear(GL_COLOR_BUFFER_BIT);

		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, texture2ID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
		glTexCoord2f(0, 0);
		glVertex2f(-1, -1);

		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);


		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(1, 1);
		glVertex2f(1, 1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);
		glEnd();

		// Disable 2D texturing
		glDisable(GL_TEXTURE_2D);

		glFlush();
	}
}




void DifficultyScreenActivity::onMouseUp(int button, int mouseX, int mouseY)										// Called when key released
{
	if(!difficultySelected){
		GameActivity* game = (GameActivity*) app->game;
		if(mouseY > camHalfHeight && mouseY < camTopQ){
			game->setDifficulty(0);
			difficultySelected = true;
		}
		if(mouseY < camHalfHeight && mouseY > camBottomQ){
			game->setDifficulty(1);
			difficultySelected = true;
		}
		if(mouseY < camBottomQ){
			game->setDifficulty(2);
			difficultySelected = true;
		}
	}
}


void DifficultyScreenActivity::onKeyUp(int key)										// Called when key released
{
	// Key released

	// Exit the start screen when the SPACE key is released, NOT pressed
	// That way the next activity starts with the space key NOT pressed
	if (difficultySelected){
		if (key == ' ')
		{
			// Space
			difficultySelected = false;
			app->setCurrentActivity(app->game);
		}
	}
}