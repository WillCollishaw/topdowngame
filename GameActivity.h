// Ensure that this file can only be included once
#pragma once
#include "Activity.h"
#include "bullet.h"
#include "Mine.h"
#include "Player.h"
#include "glfont2.h"
#include "Wall.h"
#include "Blood.h"
#include "Health.h"
#include "Civilian.h"
#include "Enemy.h"

#include<vector>

// GAME ACTIVITY
class GameActivity : public Activity
{
private:

	double camX, camY, camHalfWidth, camHalfHeight, camWidth, time, civRenderTimer, objectiveTimer, deathTime, killedCivilianX, killedCivilianY;
	double playerX, playerY, prevX, prevY, playerAngle, turretAngle, enemyAngle, enemyTurretAngle;
	bool civilianKilled, civilianInRange, disabledKeys, initialBullet, lifeLost, playerDied, enemyCollision, enemyReset, levelNeedsChanging, levelChanged;
	int savedCivilians, startingCivilians, mineDamage, enemyDamage, gameDifficulty, level, levelCompleteScore;

	//Texture ID's
	GLuint playerTextureID;
	GLuint turretTextureID;
	GLuint grassTextureID;
	GLuint treeWallTextureID;
	GLuint wallTextureID;
	GLuint fontTextureID;
	GLuint sandTextureID;
	GLuint sandbagTextureID;

	//Object Vectors
	std::vector<bullet*> bullets, enemyBullets;
	std::vector<Mine*> mines;
	std::vector<Wall*> wall;
	std::vector<Civilian*> civ;
	std::vector<Enemy*> enemy;
	std::vector<Blood*> blood;
	std::vector<Health*> health;
	Player* player1;

	glfont::GLFont font;

public:
	GameActivity(OpenGLApplication *app);

	virtual void initialise();											// Called on application start up
	virtual void shutdown();											// Called on application shut down
	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void update(double deltaT, double prevDeltaT);				// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
	// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1
	virtual void render();												// Render function
	virtual void onMouseDown(int button, int mouseX, int mouseY);		// Called when mouse button pressed
	virtual void onMouseMove(int mouseX, int mouseY);					// Called when mouse moved
	virtual void onKeyUp(int key);										// Called when key released

	void windowSpaceToWorldSpace(double &worldX, double &worldY, int winX, int winY);
	void drawBackground (double x, double y, double width, double height, double tileWidth, double tileHeight, GLuint texture); 
	double getPlayerX();
	double getPlayerY();
	bool mineCollision();
	void resolveCivilianCollision();
	void resolveHealthCollision();
	void resolveBulletsOffScreen();
	void resolveWallCollision();
	void resolveEnemyCollision();
	double getTextWidth(std::string inputString);
	int getRemainingCivs();
	void restart();
	void youDied();
	void setDifficulty(int difficulty);
	void onEnemyHit(int i);
	void enemyAI();
	void isInEnemyDistance();
	void civilianAnimation(double deltaT, double prevDeltaT);
	void updateBulletLocation(double deltaT, double prevDeltaT);
	void updateEnemyLocation(double deltaT, double prevDeltaT);
	void allCiviliansFound(double deltaT);
	void levelChange();
};