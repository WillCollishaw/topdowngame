class Civilian
{
private:
	double x, y;
	int armOut;
public:
	Civilian(double x, double y);
	~Civilian(void);
	Civilian(const Civilian &m);
	void initialise();	
	void render();
	void update(double deltaT, double prevDeltaT);	
	double getY();
	double getX();

	static GLuint civTextureID, civ2TextureID;
};