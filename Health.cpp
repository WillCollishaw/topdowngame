#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "Health.h"
#include "SOIL.h"

Health::~Health(void)
{
	glDeleteTextures(1, &healthTextureID);
}

Health::Health(double x, double y)
{
	this->x = x;
	this->y = y;
	initialise();
}

Health::Health(const Health &m)
{
	this->x = m.x;
	this->y = m.y;
}

GLuint Health::healthTextureID = -1;

void Health::initialise()
{
	if (healthTextureID = -1)
	{
		healthTextureID =  SOIL_load_OGL_texture("health.png",			// filename
			SOIL_LOAD_AUTO,											// 
			SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	
	}
}

void Health::render()
{
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, healthTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(1, 0);
	glVertex2f(x+2, y);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+2);


	glTexCoord2f(1, 0);
	glVertex2f(x+2, y);

	glTexCoord2f(1, 1);
	glVertex2f(x+2, y+2);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	
}

double Health::getY()
{
	return y;
}

double Health::getX()
{
	return x;
}