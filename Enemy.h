class Enemy
{
private:
	int health;
	double x , y, enemyTurretAngle, enemyAngle, enemyTimer, moveTimer;
public:
	~Enemy(void);
	Enemy(double x, double y, int health, double enemyTurretAngle, double enemyAngle);
	Enemy(const Enemy &m);

	void initialise();	
	void render();
	void update(double deltaT, double prevDeltaT);	
	double getY();
	double getX();
	void setX(double inputX);
	void setY(double inputY);
	int getHealth(); 
	double getTurretAngle();
	double getEnemyAngle();
	void setAngle(double inputAngle);
	void setTurretAngle(double inputAngle);
	void setHealth(int setAmount);
	void setTimer(double setTime);
	void setTurnTimer (double setTime);
	bool shouldEnemyTurn();
	bool shouldEnemyFire();

	GLuint enemyTextureID;
	GLuint enemyTurretTextureID;
};
