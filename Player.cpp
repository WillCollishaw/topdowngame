#pragma once
#include "Player.h"

Player::Player(int l, int h, int s)
{
	lives = l;
	health = h;
	score  = s;
}

int Player::getLives()
{
	return lives;
}

void Player::setLives(int setAmount)
{
	lives = setAmount;
}

int Player::getHealth()
{
	return health;
}

void Player::setHealth(int setAmount)
{
	health = setAmount;
}

//taking health away on mine collision
bool Player::hitMine(int damage)
{
	health -= damage;

	if(health <= 0)
	{
		health = 100;
		lives--;
		return true;
	}

	return false;
}
int Player::getScore()
{
	return score;
}

void Player::setScore(int setAmount)
{
	score = setAmount;
}

//Taking health away on enemy bullet collision
bool Player::shot(int damage)
{
	health -= damage;
	if (health <= 0)
	{
		health = 100;
		lives--;
		return true;
	}
	else
	{
		return false;
	}
}
//Adding 30 health on collision with health pack
void Player::healthPack()
{
	health += 30;
	if (health > 100)
	{
		health = 100;
	}
}