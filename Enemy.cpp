#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "Enemy.h"
#include "SOIL.h"

Enemy::~Enemy(void)
{
	glDeleteTextures(1, &enemyTextureID);
	glDeleteTextures(1, &enemyTurretTextureID);
}


Enemy::Enemy(double x, double y, int health, double enemyTurretAngle, double enemyAngle)
{
	this->x = x;
	this->y = y;
	this->health = health;
	this->enemyTurretAngle;
	this->enemyAngle;
	initialise();
	enemyTimer = 0.0;
	moveTimer = 0.0;

}

Enemy::Enemy(const Enemy &m)
{
	this->x = m.x;
	this->y = m.y;
	this->health = m.health;
	this->enemyTurretAngle = m.enemyTurretAngle;
	this->enemyAngle = m.enemyAngle;
}



void Enemy::initialise(){

	enemyTextureID =  SOIL_load_OGL_texture("enemyTank.png",			// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	

	enemyTurretTextureID =  SOIL_load_OGL_texture("enemyTankTop.png",			// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	
	enemyTurretAngle = 270.0;
	enemyAngle = 270.0;
	health = 100;


}

void Enemy::render()
{
	glPushMatrix();
	glTranslated(x, y, 0.0);			// Move the player to the required position
	glRotated(enemyAngle, 0.0, 0.0, 1.0);
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, enemyTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(-1, -2);

	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);


	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(1, 1);
	glVertex2f(1, 2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glDisable(GL_BLEND);

	//////////
	//turret//
	//////////
	glPushMatrix();
	glTranslated(x, y, 0.0);			// Move the player to the required position
	glRotated(enemyTurretAngle, 0.0, 0.0, 1.0);
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, enemyTurretTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(-1, -2);

	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);


	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(1, 1);
	glVertex2f(1, 2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();	// pop turret matrix
	glDisable(GL_BLEND);
}

void Enemy::update(double deltaT, double prevDeltaT)
{
	enemyTimer += deltaT;
	moveTimer += deltaT;
}

//Accessor Methods
double Enemy::getY()
{
	return y;
}

double Enemy::getX()
{
	return x;
}

void Enemy::setX(double inputX)
{
	x = inputX;
}
void Enemy::setY(double inputY)
{
	y = inputY;
}

int Enemy::getHealth()
{
	return health;
}

double Enemy::getTurretAngle()
{
	return enemyTurretAngle;
}

double Enemy::getEnemyAngle()
{
	return enemyAngle;
}

void Enemy::setAngle(double inputAngle)
{
	enemyAngle = inputAngle;
}
void Enemy::setTurretAngle(double inputAngle)
{
	enemyTurretAngle = inputAngle;
}
void Enemy::setHealth(int setAmount)
{
	health = setAmount;
}

void Enemy::setTimer(double setTime)
{
	enemyTimer = setTime;
}

void Enemy::setTurnTimer(double setTime)
{
	moveTimer = setTime;
}

//Works out if enemy should turn
bool Enemy::shouldEnemyTurn()
{
	if (moveTimer > 3)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//Works out if enemy should fire
bool Enemy::shouldEnemyFire()
{
	if (enemyTimer > 2)
	{
		return true;
	}
	else 
	{
		return false;
	}
}