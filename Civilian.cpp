#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "Civilian.h"
#include "SOIL.h"

Civilian::Civilian(double x, double y)
{
	this->x = x;
	this->y = y;
	initialise();
}

Civilian::~Civilian(void)
{
	glDeleteTextures(1, &civTextureID);
	glDeleteTextures(1, &civ2TextureID);
}

Civilian::Civilian(const Civilian &m)
{
	this->x = m.x;
	this->y = m.y; 
}

GLuint Civilian::civTextureID = -1;
GLuint Civilian::civ2TextureID = -1;

void Civilian::initialise(){
	armOut = 0;
	if(civTextureID == -1){
		civTextureID =  SOIL_load_OGL_texture("civ.png",			// filename
			SOIL_LOAD_AUTO,											// 
			SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	

		civ2TextureID =  SOIL_load_OGL_texture("civ2.png",			// filename
			SOIL_LOAD_AUTO,											// 
			SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	
	}
}

void Civilian::render()
{	
	//Every second armOut goes up by one, every other second armOut will be divisible by two, so every second the render changes
	if(armOut % 2 == 0)
	{
		// Bind our player texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, civTextureID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glEnable (GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 1.0f, 1.0f);

		glTexCoord2f(0, 0);
		glVertex2f(x, y);

		glTexCoord2f(1, 0);
		glVertex2f(x+2, y);

		glTexCoord2f(0, 1);
		glVertex2f(x, y+2);


		glTexCoord2f(1, 0);
		glVertex2f(x+2, y);

		glTexCoord2f(1, 1);
		glVertex2f(x+2, y+2);

		glTexCoord2f(0, 1);
		glVertex2f(x, y+2);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}
	else
	{
		// Bind our player texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, civ2TextureID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glEnable (GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 1.0f, 1.0f);

		glTexCoord2f(0, 0);
		glVertex2f(x, y);

		glTexCoord2f(1, 0);
		glVertex2f(x+2, y);

		glTexCoord2f(0, 1);
		glVertex2f(x, y+2);


		glTexCoord2f(1, 0);
		glVertex2f(x+2, y);

		glTexCoord2f(1, 1);
		glVertex2f(x+2, y+2);

		glTexCoord2f(0, 1);
		glVertex2f(x, y+2);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}

}

void Civilian::update(double deltaT, double prevDeltaT)
{
	armOut++;
}

double Civilian::getY()
{
	return y;
}

double Civilian::getX()
{
	return x;
}