class Wall
{
private:
	double x, y;
	GLuint wall;

public:
	Wall(const Wall &m);
	~Wall(void);
	Wall(double x, double y, GLuint inputWall);

	void render();
	double getY();
	double getX();
};

