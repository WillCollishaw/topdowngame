class Blood
{
private:
	double x,y;

public:
	~Blood(void);
	Blood(double x, double y);
	Blood(const Blood &m);
	void initialise();	
	void render();
	double getY();
	double getX();

	static GLuint bloodTextureID;
};