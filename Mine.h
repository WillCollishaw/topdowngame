class Mine
{
private:
	double x, y;
public:
	Mine(const Mine &m);
	~Mine(void);
	Mine(double x, double y);

	static GLuint mineTextureID;

	void initialise();	
	void render();	
	double getY();
	double getX();
};