#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "SOIL.h"
#include "Mine.h"

Mine::Mine(double x, double y)
{
	this->x = x;
	this->y = y;
	initialise();
}

Mine::~Mine(void)
{
	glDeleteTextures(1, &mineTextureID);
}

Mine::Mine(const Mine &m)
{
	this->x = m.x;
	this->y = m.y;
}

void Mine::render()
{
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, mineTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(1, 0);
	glVertex2f(x+4, y);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+4);


	glTexCoord2f(1, 0);
	glVertex2f(x+4, y);

	glTexCoord2f(1, 1);
	glVertex2f(x+4, y+4);

	glTexCoord2f(0, 1);
	glVertex2f(x, y+4);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

GLuint Mine::mineTextureID = -1;

void Mine::initialise()
{
	if(mineTextureID == -1){
		mineTextureID =  SOIL_load_OGL_texture("mine.png",			// filename
			SOIL_LOAD_AUTO,											// 
			SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	
	}
}

double Mine::getX()
{
	return x;
}
double Mine::getY()
{
	return y;
}