#pragma once
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "bullet.h"
#include "SOIL.h"
#include <iostream>

#define DEG_TO_RAD(x)    ((x)*(M_PI/180))
const double M_PI = 3.14156592;

bullet::~bullet(void)
{
	glDeleteTextures(1, &bulletTextureID);
}


bullet::bullet(double x, double y, double angle)
{
	this->x = x;
	this->y = y;
	this->angle = angle;

	initialise();
}

void bullet::render()
{
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, bulletTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	double width=1.5, height=0.5;

	glPushMatrix();
	glTranslated(x, y, 0.0);
	glRotated(angle+90, 0.0,0.0,1.0);

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(-width*0.5, -height*0.5);

	glTexCoord2f(1, 0);
	glVertex2f(width*0.5, -height*0.5);

	glTexCoord2f(1, 1);
	glVertex2f(width*0.5, height*0.5);


	glTexCoord2f(0, 0);
	glVertex2f(-width*0.5, -height*0.5);

	glTexCoord2f(1, 1);
	glVertex2f(width*0.5, height*0.5);

	glTexCoord2f(0, 1);
	glVertex2f(-width*0.5, height*0.5);
	glEnd();


	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

GLuint bullet::bulletTextureID =-1;

void bullet::initialise()
{
	if (bulletTextureID == -1)
	{
		bulletTextureID = SOIL_load_OGL_texture("bullet.png",		// filename
			SOIL_LOAD_AUTO,											// 
			SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
	}
}

void bullet::update(double deltaT, double prevDeltaT)
{
	float pSin = sin(DEG_TO_RAD(-angle));
	float pCos = cos(DEG_TO_RAD(-angle));
		x += 75 * deltaT * pSin;
		y += 75 * deltaT * pCos;

}

double bullet::getX(){
	return x;
}

double bullet::getY(){
	return y;
}