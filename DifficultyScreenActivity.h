#pragma once
#include "Activity.h"

class DifficultyScreenActivity : public Activity
{
private:
	GLuint textureID, texture2ID;

	double camHalfHeight, camBottomQ, camTopQ;
	bool difficultySelected;

public:
	DifficultyScreenActivity(OpenGLApplication *app);

	// ACTIVITY METHODS
	virtual void initialise();											// Called on application start up
	virtual void shutdown();											// Called on application shut down

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void render();												// Render function

	virtual void onMouseUp(int button, int mouseX, int mouseY);	
	virtual void onKeyUp(int key);
};