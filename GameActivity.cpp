
/*
GameActivity (Level 1) implementation
*/
#pragma once
#pragma comment(lib, "winmm.lib")

#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "GameActivity.h"

#include "SOIL.h"
#include "OpenGLApplication.h"			// Needed to access member functions and variables from OpenGLApplication
#include "endScreenActivity.h"
#include <sstream>

using namespace std;

#include "glfont2.h"
using namespace glfont;
GLFont myfont;

const double M_PI = 3.14156592;

#define DEG_TO_RAD(x)    ((x)*(M_PI/180.0))
#define RAD_TO_DEG(x)    ((x)*(180.0/M_PI))
#define VIEW_SIZE 50.0					// The height of the view in WORLD SPACE
#define CAMERA_MOVEMENT_SPEED 10.0
#define PLAYER_MOVEMENT_SPEED 20.0
#define PLAYER_TURN_SPEED 150.0

/*
Map Matrix Numbers Level 1
0 = background (grass)
1 = tree (wall)
2 = mine
3 = wood wall
4 = civilian 
5 = enemy tank
6 = health pack
7 = reinforcements
*/
int map [50][80]={
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,6,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,6,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,2,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,7,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1},
	{1,0,0,5,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1},
	{1,0,0,0,0,0,1,0,0,1,0,1,0,2,1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1},
	{1,0,0,0,0,0,1,1,0,0,0,0,0,2,1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,6,0,5,0,1,1},
	{1,0,0,0,0,0,1,1,1,0,0,0,0,2,1,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,2,0,0,0,2,1,1},
	{1,0,0,0,0,0,1,1,1,1,0,0,0,0,1,0,0,0,7,0,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,7,0,0,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,7,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,0,2,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,5,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,2,2,2,0,1,1,1,1,1,0,0,0,1,0,0,0,0,5,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,5,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,7,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,5,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,2,2,2,0,0,0,0,2,2,2,2,2,1,1,0,6,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,2,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1},
	{1,0,0,5,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,0,0,2,0,0,1,1,1,1,1,1},
	{1,0,0,0,0,0,1,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,5,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1},
	{1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,5,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,1,1,0,0,0,0,5,0,0,0,0,1,1,1,1},
	{1,0,0,2,0,0,1,0,0,0,0,5,0,0,7,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
	{1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,5,0,0,0,0,0,0,1,1},
	{1,0,5,0,0,0,1,1,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,1,1,1,0,0,0,0,0,7,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,7,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,7,0,0,0,0,2,2,2,0,0,1,1,1,0,0,0,0,0,5,0,0,0,0,0,0,0,1},
	{1,0,0,0,2,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,0,6,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,2,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,2,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2,0,2,0,0,0,0,0,2,2,2,2,2,2,0,0,0,1,1,1,0,0,0,5,0,0,0,0,0,5,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2,0,2,0,0,0,5,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,4,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{3,0,0,0,3,3,3,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,5,0,0,0,0,0,0,1},
	{3,0,0,0,0,0,3,1,1,1,1,1,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{3,0,0,0,0,0,3,1,1,1,1,1,1,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{3,0,0,0,0,0,3,1,1,1,1,1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,6,0,0,0,0,0,0,1},
	{3,0,0,0,0,0,3,1,1,1,1,1,0,0,0,0,1,1,6,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,0,2,0,0,2,0,2,0,0,2,0,2,1},
	{3,0,0,0,0,0,3,1,1,1,1,1,1,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,2,2,0,0,2,0,2,0,0,2,2,2,1},
	{3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};

/*
Map Matrix Numbers Level 2
0 = background (sand)
1 = wall (sandbag)
2 = mine
3 = wood wall
4 = civilian 
5 = enemy tank
6 = health pack
7 = reinforcements
*/
int map2 [50][80]={
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,6,0,0,0,0,0,0,0,0,2,2,2,0,0,0,1,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,6,0,4,0,6,0,1,0,6,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,2,4,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,5,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,0,0,2,2,2,0,0,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,5,0,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,2,2,2,2,2,0,1,0,0,1,0,0,5,0,0,0,2,0,0,5,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,0,0,0,2,2,2,2,0,0,2,0,0,0,0,1},
	{1,0,2,0,2,0,2,0,1,0,0,1,0,0,0,0,0,2,4,2,0,0,0,0,0,1,0,1,1,1,1,0,1,1,0,1,1,1,0,0,7,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,2,0,2,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,2,0,2,0,2,0,1,0,0,1,0,0,5,0,0,0,2,0,0,5,0,0,0,1,0,0,0,1,1,0,1,1,0,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,0,0,0,2,6,2,2,0,0,2,0,0,0,0,1},
	{1,0,2,2,2,2,2,0,1,0,0,1,0,0,0,0,1,1,1,1,1,0,0,0,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,2,0,2,0,0,0,2,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,5,0,0,0,1,0,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,0,0,0,0,0,0,0,0,2,2,0,0,2,0,0,6,0,1},
	{1,0,0,0,0,0,0,0,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,2,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1},
	{1,0,0,0,7,0,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,2,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,7,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,2,0,1},
	{1,2,2,0,2,0,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,5,0,0,0,2,1},
	{1,0,2,0,2,0,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,0,5,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,7,0,0,0,0,1},
	{1,0,2,0,2,0,2,0,1,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,2,2,2,0,0,0,0,2,2,2,2,2,0,0,0,0,0,0,2,2,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,2,0,2,0,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,0,1,0,1,0,0,0,0,0,0,0,1,2,2,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,6,2,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
	{1,2,2,0,2,0,2,2,1,1,0,0,0,0,0,7,0,0,0,0,0,0,5,0,0,0,0,1,0,1,2,2,0,1,0,1,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,4,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,6,4,0,1,0,1,0,0,0,7,0,0,0,1,2,0,0,2,2,0,0,2,2,2,2,2,0,0,0,0,0,0,2,2,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,7,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,1,0,1,2,2,2,1,0,1,0,0,0,0,0,0,0,1,2,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,0,1,0,0,0,0,0,0,0,1,2,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,2,0,0,0,0,5,0,0,0,7,0,0,5,0,0,0,0,0,2,2,1,1,1,1,1,1,0,0,0,0,7,0,0,0,0,1},
	{1,2,0,0,0,0,0,2,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,5,0,0,2,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
	{1,2,0,0,0,0,0,2,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,2,0,2,0,0,0,0,0,0,0,2,0,0,0,0,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,5,0,0,0,1},
	{1,2,0,0,0,0,0,2,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,2,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,5,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,2,2,1,1,0,2,2,2,0,2,2,2,0,2,2,2,0,1},
	{1,2,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,1,1,0,0,2,0,0,0,0,2,0,0,2,0,0,0,1},
	{1,1,2,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,2,2,2,0,0,2,0,0,2,0,0,0,1},
	{1,1,1,2,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,2,0,0,2,0,0,2,0,0,0,1},
	{1,1,1,1,2,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,2,4,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,2,2,2,0,0,2,0,0,2,2,2,0,1},
	{1,1,1,1,1,2,0,0,1,1,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,1,1,1,2,0,0,0,1,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,1,1,2,0,0,0,0,1,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
	{1,1,2,0,0,0,0,0,1,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1},
	{1,2,0,0,5,0,0,0,1,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,5,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,7,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,7,0,0,0,1,1,1,0,0,0,2,0,2,0,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,1,0,0,0,2,0,2,0,0,0,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,0,0,3},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,3},
	{1,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,3,0,0,0,0,0,3},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,4,0,0,0,0,3,0,0,0,0,0,3},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,3,0,0,0,0,0,3},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,3},
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3}
};

int mapHeight = 50;
int mapWidth = 80;

GameActivity::GameActivity(OpenGLApplication *app)
	: Activity(app)		// Call super constructor
{
	// Initialse camera and player positions to be a the origin
	playerX = 5.0;
	playerY = -180.0;
	playerAngle = 0.0;
	turretAngle = 0.0;
	enemyAngle = 90.0;
	enemyTurretAngle = 90.0;
}

// Initialise the activity; called at application start up
void GameActivity::initialise()
{
	//Initialising default variables
	time = 0;
	savedCivilians = 0;
	startingCivilians = 0;
	levelCompleteScore = 0;
	level = 1;
	civilianKilled = false;
	civilianInRange = false;
	disabledKeys = false;
	initialBullet = false;
	lifeLost = false; 
	enemyCollision = false;
	levelChanged = false;
	levelNeedsChanging = false;
	deathTime = -1;
	civRenderTimer = 0;
	enemyReset = false;
	objectiveTimer = 0.0;
	playerDied = false;

	// Load the player image as a texture using the SOIL library
	playerTextureID = SOIL_load_OGL_texture("player3.png",		// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y

	turretTextureID = SOIL_load_OGL_texture("playertop.png",    // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	

	grassTextureID = SOIL_load_OGL_texture("grass.jpg",    // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	

	treeWallTextureID =  SOIL_load_OGL_texture("tree.png",    // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);	

	wallTextureID =  SOIL_load_OGL_texture("wall.png",	        // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);

	sandTextureID =  SOIL_load_OGL_texture("sand.jpg",	        // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);

	sandbagTextureID =  SOIL_load_OGL_texture("sandbag.png",	        // filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);

	glGenTextures(1, &fontTextureID);
	font.Create("Arial.glf", fontTextureID);

	//map objects

	for (int i = 0; i < mapWidth ; i++){
		for (int k = 0; k < mapHeight; k++){
			if (map [k][i] == 1){
				wall.push_back(new Wall (i*4, -k*4, treeWallTextureID));
			}
			if (map[k][i] == 2){
				mines.push_back(new Mine(i*4, -k*4));
			}
			if (map[k][i] == 3){
				wall.push_back(new Wall (i*4, -k*4, wallTextureID));
			}
			if (map[k][i] == 4){
				civ.push_back(new Civilian (i*4, -k*4));
				startingCivilians++;
			}
			if(map[k][i] == 5){
				enemy.push_back(new Enemy (i*4, -k*4, 100, enemyTurretAngle, enemyAngle));
			}
			if (map[k][i] == 6){
				health.push_back(new Health (i*4, -k*4));
			}
		}
	}

}
void GameActivity::shutdown()
{
	// Shutdown the activity; called at application finish

	// Delete the texture
	glDeleteTextures(1, &playerTextureID);
	glDeleteTextures(1, &turretTextureID);
	glDeleteTextures(1, &grassTextureID);
	glDeleteTextures(1, &treeWallTextureID);
	glDeleteTextures(1, &wallTextureID);

	font.Destroy();
	glDeleteTextures(1, &fontTextureID);
}



void GameActivity::onSwitchIn()
{
	// Activity switch in
	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to black

}

void GameActivity::onReshape(int width, int height)
{
	// If you need to do anything when the screen is resized, do it here
	glViewport(0,0,width,height);						// Reset The Current Viewport
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	camHalfWidth = VIEW_SIZE*0.5*aspect;
	camHalfHeight = VIEW_SIZE*0.5;
	camWidth = VIEW_SIZE*aspect;
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect,  -VIEW_SIZE*0.5, VIEW_SIZE*0.5);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void GameActivity::update(double deltaT, double prevDeltaT)
{
	// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
	// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1

	// Update the simulation here
	// step the simulation forward by the amount of time specified by deltaT
	//
	// If you need to do different things depending on whether or not keys are pressed, etc,
	// get the member variable inputState and use its isKeyPressed(), getMouseX(), getMouseY() and isMouseButtonPressed() methods
	// to determine the state of the keys

	//Changing Level
	levelChange();
	//Timers
	time += deltaT;
	civRenderTimer += deltaT;

	//Sine of player angle
	float pSin = sin(DEG_TO_RAD(-playerAngle));
	//Cosine of player angle
	float pCos = cos(DEG_TO_RAD(-playerAngle));

	//Saving previous co-ords for collision detection
	prevX = playerX, prevY = playerY;

	//If keys are not disabled. Normal game controls are activated
	if (!disabledKeys)
	{
		// WASD control player
		if(inputState->isKeyPressed('A'))
		{
			playerAngle += PLAYER_TURN_SPEED * deltaT;
		}
		if(inputState->isKeyPressed('D'))
		{		
			playerAngle -= PLAYER_TURN_SPEED * deltaT;
		}
		if(inputState->isKeyPressed('W'))
		{
			playerX += PLAYER_MOVEMENT_SPEED * deltaT * pSin;
			playerY += PLAYER_MOVEMENT_SPEED * deltaT * pCos;
		}
		if(inputState->isKeyPressed('S'))
		{
			playerX -= PLAYER_MOVEMENT_SPEED * deltaT * pSin;
			playerY -= PLAYER_MOVEMENT_SPEED * deltaT * pCos;
		}

		//If keys are not disabled (game is still in play) bullet and enemy location will carry on updating)
		if (!levelNeedsChanging)
		{
			updateBulletLocation(deltaT, prevDeltaT);
			updateEnemyLocation (deltaT, prevDeltaT);
		}
	}

	//If a player dies keys are disabled, and a new timer is created until player respawns
	if(playerDied)
	{
		disabledKeys = true;
		lifeLost = true;
		if (player1->getLives() > 0)
		{
			deathTime = time;
		}
	}
	//Updating to see if the player has collided with a mine
	if(mineCollision())
	{
		//hitMine method returns true if the mine damage kills the player 
		if(player1->hitMine(mineDamage))
		{
			disabledKeys = true;
			lifeLost = true;
			deathTime = time;
		}
	}
	//If player Loses a life
	if (lifeLost)
	{
		//Only run if player has lives left
		if (player1->getLives() > 0)
		{
			//playerDied set to false to stop the deathTime variable reseting 
			playerDied = false;
			if (time > deathTime+3){
				lifeLost = false;
				deathTime = 0; 
				youDied();
			}
		}
	}
	//Ends game if player hits zero lives remaining
	if (player1->getLives() <= 0){
		disabledKeys = true;
	}

	//Collision Methods updating
	resolveWallCollision();
	resolveEnemyCollision();
	resolveCivilianCollision();
	resolveHealthCollision();
	resolveBulletsOffScreen();

	//If player is not colliding with an enemy, The enemy will move around, else the enemy will stand still
	if(!enemyCollision && !levelNeedsChanging){
		enemyAI();
	}
	//reseting enemyCollision variable for next frame
	enemyCollision = false;

	//Updating the camera position to centre the player ( the camera follows the players position )
	camX = playerX;
	camY = playerY;

	//Checking if all civilians are found, if they have all been found, base must be reached and reinforcements are added
	allCiviliansFound(deltaT);

	//Checks that the player is within a certain range of an enemy. If so the enemy will aim and shoot at the player
	isInEnemyDistance();

	//Activating civilian animation (arm moving)
	civilianAnimation(deltaT, prevDeltaT);
}


void GameActivity::render()
{
	// OpenGL render calls go in this method

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();// Reset The Modelview Matrix
	glPushMatrix(); //push camera matrix
	// Use the negated camera position as a translation; effectively we move the world and the camera so that the camera is at 0,0,0
	glTranslated(-camX, -camY, 0.0);


	//background
	double CAM_LOWER_X = camX-camHalfWidth;
	double CAM_LOWER_Y = camY-camHalfHeight;
	if (level == 1)
	{
		drawBackground(CAM_LOWER_X, CAM_LOWER_Y,camHalfWidth*2,camHalfHeight*2,7,7,grassTextureID);
	}
	else if (level == 2)
	{
		drawBackground(CAM_LOWER_X, CAM_LOWER_Y,camHalfWidth*2,camHalfHeight*2,7,7,sandTextureID);
	}
	//mines
	for (int i = 0; i < mines.size() ; i++){
		mines.at(i)->render();
	}
	//health packs
	for (int i = 0; i < health.size(); i++){
		health.at(i)->render();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// player
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glPushMatrix(); //push player matrix
	glTranslated(playerX, playerY, 0.0);			// Move the player to the required position
	glRotated(playerAngle, 0.0, 0.0, 1.0);
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, playerTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(-1, -2);

	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);


	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(1, 1);
	glVertex2f(1, 2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);
	glEnd();

	// Disable 2D texturing
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();	// pop player matrix

	//////////////////////
	//renders the turret//
	//////////////////////
	glPushMatrix(); //push turret matrix
	glTranslated(playerX, playerY, 0.0);			// Move the player to the required position
	glRotated(turretAngle, 0.0, 0.0, 1.0);
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, turretTextureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);


	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex2f(-1, -2);

	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);


	glTexCoord2f(1, 0);
	glVertex2f(1, -2);

	glTexCoord2f(1, 1);
	glVertex2f(1, 2);

	glTexCoord2f(0, 1);
	glVertex2f(-1, 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();	// pop turret matrix
	glDisable(GL_BLEND);

	////////////////////////////////////////////////////////////////////////////////////////////////////

	//bullets
	//Fixes a bug that let players jump through walls when first bullet was fired
	if (!initialBullet){
		bullets.push_back(new bullet(playerX, playerY, turretAngle));
		bullets.erase(bullets.begin());
		initialBullet = true;
	}

	if (!disabledKeys){
		for (int i = 0; i < bullets.size(); i++){
			bullets.at(i)->render();
		}
		for (int i = 0; i < enemyBullets.size(); i++){
			enemyBullets.at(i)->render();
		}
	}
	//Enemys
	for (int i  =0; i < enemy.size(); i++){
		enemy.at(i)->render();
	}
	//walls
	for (int i = 0; i < wall.size(); i++){
		wall.at(i)->render();
	}

	//civilians
	for (int i = 0; i < civ.size(); i++){
		civ.at(i)->render();
	}
	//Blood
	for (int i  =0; i < blood.size(); i++){
		blood.at(i)->render();
	}

	glPopMatrix();	// Pop camera matrix

	//creating interactive string variables
	ostringstream livesstream;
	livesstream << "Lives = " << player1->getLives();
	string lifestring = livesstream.str();

	ostringstream scorestream;
	scorestream<< "Score = " << player1->getScore();
	string scorestring = scorestream.str();

	ostringstream remainingstream;
	remainingstream<< "Remaining Civilians = " << getRemainingCivs();
	string remainingstring = remainingstream.str();

	ostringstream levelstream;
	levelstream << "Level " << level << " Complete!";
	string levelstring = levelstream.str();

	//text box
	glPushMatrix(); //push textbox matrix

	double aspect = app->getAspectRatio();
	glTranslated(-VIEW_SIZE*0.5*aspect,VIEW_SIZE*0.5,0);
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex2f(-camWidth, -3);

	glVertex2f(-camWidth, 3);

	glVertex2f(camWidth, 3);

	glVertex2f(camWidth, -3);

	glEnd();
	glPopMatrix(); // pop textbox matrix


	//life bar
	glPushMatrix(); // push life bar matrix
	glTranslated(-5.0 , 23.25 ,0.0);
	glBegin(GL_QUADS);
	if (player1->getHealth() > 50){
		glColor3f(0.0f, 1.0f, 0.0f);
	}
	else
	{
		glColor3f(1.0f, 0.0f, 0.0f);
	}

	glVertex2f(0, -1);

	glVertex2f(0, 1);

	glVertex2f(player1->getHealth()/12.5, 1);

	glVertex2f(player1->getHealth()/12.5, -1);

	glEnd();
	glPopMatrix(); // pop life bar matrix

	//////////TEXT////////////
	glPushMatrix(); //pop text matrix

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	font.Begin();

	glColor3f(1.0, 1.0, 1.0);
	if (level == 1)
	{
		font.DrawString("Level 1", 0.1, 23.0, 25.0);	//top right
	}
	else if(level == 2)
	{
		font.DrawString("Level 2", 0.1, 23.0, 25.0);	//top right
	}
	font.DrawString(scorestring, 0.1, 3, 25.0);		//top centre
	font.DrawString(lifestring, 0.1, -33.0, 25.0);	//top left
	font.DrawString("Health = ", 0.1, -17.0, 25.0);//top left under lives

	if (time < 3 && !lifeLost){
		if(level == 1){
			font.DrawString("Level 1", 0.1, -getTextWidth("Level 1")/20, 10.0);	//top right
			font.DrawString("Save the Civilians", 0.1, -getTextWidth("Save the Civilians")/20,0.0);
		}
		else if (level == 2)
		{
			font.DrawString("Level 2", 0.1, -getTextWidth("Level 1")/20, 10.0);	//top right
			font.DrawString("Save the Civilians", 0.1, -getTextWidth("Save the Civilians")/20,0.0);

		}
	} else if(inputState->isKeyPressed('Q') && !levelNeedsChanging){
		if(getRemainingCivs() <= 0){
			font.DrawString("All Civilians saved. Return to base!", 0.05, -33.0 ,22.0);
		}
		else{
			font.DrawString("Save the Civilians", 0.05, -33.0 ,22.0);
			font.DrawString(remainingstring, 0.05, -33.0 ,20.0);
		}
	}
	else if (inputState->isKeyPressed('C')  && !levelNeedsChanging)
	{
		font.DrawString("Controls", 0.05,-33.0,22.0);
		font.DrawString("'W' key to move the tank forward", 0.05,-33.0,20.0);
		font.DrawString("'S' key to reverse the tank", 0.05,-33.0,18.0);
		font.DrawString("'D' key to rotate the tank clockwise", 0.05,-33.0,16.0);
		font.DrawString("'A' key to rotate the tank anti-clockwise", 0.05,-33.0,14.0);
		font.DrawString("Any mouse button to shoot", 0.05,-33.0,12.0);
		font.DrawString("'F1' to quit early", 0.05,-33.0,10.0);
		font.DrawString("'Esc' to close the game", 0.05,-33.0,8.0);
	}

	else if (!lifeLost){
		font.DrawString("Hold 'Q' to view objective list or 'C' for controls", 0.05, -33.00, 22.00);
	}

	if(civilianKilled == true)
	{
		font.DrawString("GAME OVER", 0.2, getTextWidth("GAME OVER")/-10, 15.0);
		font.DrawString("A civilian was killed",0.1,getTextWidth("A civilian was killed")/-20, 5.0);
		font.DrawString(scorestring, 0.1, getTextWidth(scorestring)/-20, 0.0);
		font.DrawString("Press 'R' to restart", 0.1, getTextWidth("Press 'R' to restart")/-20, -5.0);
		font.DrawString("Press 'Q' to quit", 0.1, getTextWidth("Press 'Q' to quit")/-20, -10.0);
	}
	if(player1->getLives() <= 0)
	{
		font.DrawString("GAME OVER", 0.2, getTextWidth("GAME OVER")/-10, 15.0);
		font.DrawString("All lives were lost",0.1,getTextWidth("All lives were lost")/-20, 5.0);
		font.DrawString(scorestring, 0.1, getTextWidth(scorestring)/-20, 0.0);
		font.DrawString("Press 'R' to restart", 0.1, getTextWidth("Press 'R' to restart")/-20, -5.0);
		font.DrawString("Press 'Q' to quit", 0.1, getTextWidth("Press 'Q' to quit")/-20, -10.0);
	}

	if(levelNeedsChanging)
	{
		disabledKeys = true;
		font.DrawString(levelstring, 0.2, getTextWidth(levelstring)/-10, 15.0);
		if (level < 2) {
			font.DrawString("Press 'R' to play the next level", 0.1, getTextWidth("Press 'R' to play the next level")/-20, -5.0);
		}
		else 
		{
			font.DrawString(scorestring, 0.1, getTextWidth(scorestring)/-20, 0.0);
			font.DrawString("Final Level Complete", 0.1, getTextWidth("Final Level Complete")/-20, -5.0);
			font.DrawString("More levels in future updates!", 0.1, getTextWidth("More levels in future updates!")/-20, -10.0);	
			font.DrawString("Press 'R' to Play Again", 0.1, getTextWidth("Press 'R' to Play Again")/-20, -20.0);
		}
		font.DrawString("Press 'Q' to quit", 0.1, getTextWidth("Press 'Q' to quit")/-20, -15.0);
	}

	if(civilianInRange == true && civilianKilled == false & levelNeedsChanging == false)
	{
		font.DrawString("Press 'R' to save the civilian", 0.05, getTextWidth("Press 'R' to save the civilian")/-40, 10.0);
	}

	if (lifeLost && player1->getLives() > 0)
	{
		font.DrawString("You Died!", 0.2, getTextWidth("You Died!")/-10, 7.0);
		font.DrawString("Respawning...", 0.1, getTextWidth("Respawning...")/-20, -3.0);
	}

	if(getRemainingCivs() <= 0)
	{
		if (objectiveTimer < 3)
		{
			font.DrawString("Objective Update! - Return to base", 0.1, -getTextWidth("Objective Update! - Return to base")/20, 10.0);
			font.DrawString("Reinforcements on the way", 0.1, -getTextWidth("Reinforcements on the way")/20,0.0);
		}
	}
	glDisable(GL_BLEND);
	glPopMatrix(); //pop text matrix

}


void GameActivity::onMouseDown(int button, int mouseX, int mouseY)
{
	// This method will be invoked when a mouse button is pressed
	// button: 0=LEFT, 1=MIDDLE, 2=RIGHT
	// mouseX and mouseY: position

	//Fires bullets on click of any button
	bullets.push_back(new bullet(playerX, playerY, turretAngle));
}

void GameActivity::onMouseMove(int mouseX, int mouseY)
{
	// This method will be invoked when the mouse is moved
	//Enables player to move the tanks turret with the mouse unless keys are disabled
	if (!disabledKeys){
		double mouseWorldX, mouseWorldY;
		windowSpaceToWorldSpace(mouseWorldX, mouseWorldY, mouseX, mouseY);

		double XDifference = mouseWorldX - playerX;
		double YDifference = mouseWorldY - playerY;

		turretAngle = - atan2 (XDifference,YDifference) * 180.0 / M_PI;	
	}
}


void GameActivity::onKeyUp(int key)										// Called when key released
{
	// Key released

	// Exit the start screen when the SPACE key is released, NOT pressed
	// That way the next activity starts with the space key NOT pressed
	if (key == VK_F1)
	{
		// F1; switch to end screen activity
		app->setCurrentActivity(app->endScreen);
	}

	//If player or civilian is killed game keys are disabled and restart and quit keys are activated
	if(disabledKeys == true && !levelNeedsChanging)
	{
		if(key == 'R')
		{
			//Game Restart
			restart();
			app->setCurrentActivity(app->startScreen);
		}

		if (key == 'Q')
		{
			//Game End
			app->finish();
		}
	}
	if (levelNeedsChanging)
	{
		if(key == 'R')
		{
			if (level < 2){
				//Loads next level
				levelChanged = true;
			}
			else{
				restart();
			}
		}

		if (key == 'Q')
		{
			//Game End
			app->finish();
		}
	}
}


void GameActivity::windowSpaceToWorldSpace(double &worldX, double &worldY, int winX, int winY)
{
	//This method works out the cursors location in the game world.
	double projection[16], modelview[16];
	int viewport[4];
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetIntegerv(GL_VIEWPORT, viewport);

	double worldZ;
	gluUnProject((double)winX, (double)winY, 0.0, modelview, projection, viewport, &worldX, &worldY, &worldZ);

	worldX+=camX;
	worldY+=camY;
}

void GameActivity::drawBackground (double x, double y, double width, double height, double tileWidth, double tileHeight, GLuint texture){
	//This method creates the background for my maps, my objects will be rendered on top of my maps
	double tileWidthR = 1/tileWidth;
	double tileHeightR = 1/tileHeight;
	// Bind our player texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(x*tileWidthR, y*tileHeightR);
	glVertex2f(x, y);

	glTexCoord2f((x+width)*tileWidthR, y*tileHeightR);
	glVertex2f(x+width, y);

	glTexCoord2f(x*tileWidthR, (y+height)*tileHeightR);
	glVertex2f(x, y+height);


	glTexCoord2f((x+width)*tileWidthR, y*tileHeightR);
	glVertex2f(x+width, y);

	glTexCoord2f((x+width)*tileWidthR, (y+height)*tileHeightR);
	glVertex2f(x+width, y+height);

	glTexCoord2f(x*tileWidthR, (y+height)*tileHeightR);
	glVertex2f(x, y+height);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

double GameActivity::getPlayerX()
{
	return playerX;
}

double GameActivity::getPlayerY()
{
	return playerY;
}
bool GameActivity::mineCollision()
{
	//Bounding box collision for my mines
	bool collision = false;
	for (int i = 0; i < mines.size() ; i++){
		Mine* mine = mines.at(i);
		float playerLowerX = playerX - 1, playerUpperX = playerX + 1;
		float mineLowerX = mine->getX(), mineUpperX = mineLowerX + 4;

		if (playerLowerX < mineUpperX && playerX > mineLowerX){
			// Collision in X
			float playerLowerY = playerY - 1, playerUpperY = playerY + 1;
			float mineLowerY = mine->getY(), mineUpperY = mineLowerY + 4;

			if (playerLowerY < mineUpperY && playerUpperY >mineLowerY){
				//Collision in Y (and X); the boxes intersect
				mines.erase(mines.begin()+i);
				collision = true;
				PlaySound("mineSound.WAV", NULL, SND_ASYNC);
			}
		}
	}
	return collision; 
}

void GameActivity::resolveCivilianCollision()
{
	float playerLowerX = playerX - 1, playerUpperX = playerX + 1;
	float playerLowerY = playerY - 1, playerUpperY = playerY + 1;

	//Bounding box collision detection checking if players bullets hit any civilians
	for (int i = civ.size()-1; i>=0; i--){
		Civilian* c = civ.at(i);
		for (int j = bullets.size()-1; j>=0; j--){
			bullet* b = bullets.at(j);
			float civLowerX = c->getX(), civUpperX = civLowerX + 2;
			float civLowerY = c->getY(), civUpperY = civLowerY + 2;

			if(b->getX() < civUpperX && b->getX() > civLowerX){
				if(b->getY() < civUpperY && b->getY() > civLowerY)
				{
					//Erasing bullet & civilian, rendering blood splat
					bullets.erase(bullets.begin()+j);
					killedCivilianY = c->getY();
					killedCivilianX = c->getX();
					civ.erase(civ.begin()+i);
					blood.push_back(new Blood(killedCivilianX,killedCivilianY));
					civilianKilled = true;
					disabledKeys = true;
				}
			}
		}
	}
	//Bounding box collision detection checking if players tank hit any civilians
	civilianInRange = false;
	for (int i = civ.size()-1; i>=0; i--){
		Civilian* c = civ.at(i);
		float civLowerX = c->getX(), civUpperX = civLowerX + 2;
		float civLowerY = c->getY(), civUpperY = civLowerY + 2;
		if (playerLowerX < civUpperX && playerX > civLowerX){
			// Collision in X
			if (playerLowerY < civUpperY && playerUpperY >civLowerY){
				civilianKilled = true;
				killedCivilianY = c->getY();
				killedCivilianX = c->getX();
				civ.erase(civ.begin()+i);
				blood.push_back(new Blood(killedCivilianX,killedCivilianY));
				disabledKeys = true;
			}
		}
		//If player is within a certain range of a civilian they will have the option to save them
		if(playerX < c->getX()+8 && playerX > c->getX()-8){
			if(playerY < c->getY()+8 && playerY > c->getY()-8){
				civilianInRange = true;
				if (inputState->isKeyPressed('R')){
					civ.erase(civ.begin()+i);
					savedCivilians++;
					player1->setScore(player1->getScore() + 200);
					civilianInRange = false;
				}
			}	
		}
	}
}

void GameActivity::resolveHealthCollision()
{

	//Bounding box collision detection checking if players tank hit any health packs
	float playerLowerX = playerX - 1, playerUpperX = playerX + 1;
	float playerLowerY = playerY - 1, playerUpperY = playerY + 1;

	for (int i = 0; i < health.size(); i++)
	{
		Health* h = health.at(i);
		float healthLowerX = h->getX(), healthUpperX = healthLowerX + 2;
		float healthLowerY = h->getY(), healthUpperY = healthLowerY + 2;

		if (playerLowerX < healthUpperX && playerUpperX > healthLowerX )
		{
			if(playerLowerY < healthUpperY && playerUpperY > healthLowerY)
			{

				player1->healthPack();
				health.erase(health.begin()+i);
			}
		}
	}
}

void GameActivity::resolveBulletsOffScreen()
{
	//Deletes bullets if they go out of a players view
	for (int j = bullets.size()-1; j >= 0 ; j--)
	{
		bullet* b = bullets.at(j);
		if(b->getX() > playerX+camHalfWidth || b->getX() < playerX-camHalfWidth||b->getY() > playerY+camHalfHeight || b->getY() < playerY-camHalfHeight)
		{
			bullets.erase(bullets.begin()+j);
		}
	}
}

void GameActivity::resolveWallCollision()
{
	float playerLowerX = playerX - 1, playerUpperX = playerX + 1;
	float playerLowerY = playerY - 1, playerUpperY = playerY + 1;

	for (int i = wall.size()-1; i >=0 ; i--){
		Wall* w = wall.at(i);
		float wallLowerX = w->getX(), wallUpperX = wallLowerX + 4;
		float wallLowerY = w->getY(), wallUpperY = wallLowerY + 4;

		//Deletes bullets if they hit a wall
		for (int j = bullets.size()-1; j >= 0 ; j--){
			bullet* b = bullets.at(j);
			if(b->getX() < wallUpperX && b->getX() > wallLowerX){
				if (b->getY() < wallUpperY && b->getY() > wallLowerY){
					bullets.erase(bullets.begin()+j);
				}
			} 
		}
		//Deletes enemy bullets if they hit a wall
		for (int j = 0; j<enemyBullets.size(); j++){
			bullet* b = enemyBullets.at(j);
			if(b->getX() < wallUpperX && b->getX() > wallLowerX){
				if (b->getY() < wallUpperY && b->getY() > wallLowerY){
					enemyBullets.erase(enemyBullets.begin()+j);
				}
			} 
		}
		//Player collision with walls
		if (playerLowerX < wallUpperX && playerUpperX > wallLowerX ){
			// Collision in X

			if(playerLowerY < wallUpperY && playerUpperY > wallLowerY){
				// Collision in Y (and X); the boxes intersect

				// Wall Sliding
				// The code below computes the minimum translation vector,
				// which is used to push the player back out of the obstacle
				float dxLeft = wallUpperX - playerLowerX;   // Overlap on the left of the player
				float dxRight = wallLowerX - playerUpperX;	// Overlap on the right of the player (negative)

				float dxBottom = wallUpperY - playerLowerY; // Overlap on the bottom of the player
				float dxTop = wallLowerY - playerUpperY;	// Overlap on the top of the player (negative)

				// Now we compute the minimum overlap; we move the object by the minimum amount to avoid colliding
				float bestDX = dxLeft < -dxRight  ?  dxLeft : dxRight;
				float bestDY = dxBottom < -dxTop  ?  dxBottom : dxTop;

				// Get the amount of overlap in each axis
				float overlapX = fabs(bestDX), overlapY = fabs(bestDY);

				if (overlapX < overlapY) {
					// Overlap in X is less; we move in X
					playerX += bestDX;
				}
				else {
					// Overlap in Y is less; move in Y
					playerY += bestDY;
				}
			}
		}
	}
}


void GameActivity::resolveEnemyCollision()
{

	float playerLowerX = playerX - 1, playerUpperX = playerX + 1;
	float playerLowerY = playerY - 1, playerUpperY = playerY + 1;

	for (int i = 0; i < enemy.size() ; i++){
		Enemy* e = enemy.at(i);
		float enemyLowerX = e->getX() - 1.5, enemyUpperX = e->getX() + 1.5;
		float enemyLowerY = e->getY() - 2, enemyUpperY = e->getY() + 2;
		if (playerLowerX < enemyUpperX && playerUpperX > enemyLowerX ){
			// Collision in X
			if(playerLowerY < enemyUpperY && playerUpperY > enemyLowerY){
				playerX = prevX;
				playerY = prevY;
				enemyCollision = true;
			}

		}
		//Deletes bullets and applys damage if they hit an enemy 
		for (int k = 0; k<bullets.size(); k++){
			bullet* b = bullets.at(k);
			if(b->getX() < enemyUpperX && b->getX() > enemyLowerX){
				if (b->getY() < enemyUpperY && b->getY() > enemyLowerY){
					bullets.erase(bullets.begin()+k);
					onEnemyHit(i);
				}
			} 
		}
		//Deletes bullets and applys damage if enemy bullets hit player
		for (int i = 0; i<enemyBullets.size(); i++){
			bullet* b = enemyBullets.at(i);
			if(b->getX() < playerUpperX && b->getX() > playerLowerX){
				if(b->getY() < playerUpperY && b->getY() > playerLowerY){
					playerDied = player1->shot(enemyDamage);
					enemyBullets.erase(enemyBullets.begin()+i);
				}
			}
		}
	}
}

double GameActivity::getTextWidth(string inputString)
{
	//Works out the width of text so text can be centralised
	double width = 0;
	for(int i = 0; i < inputString.size(); i++){
		width += font.GetCharWidthA((int)inputString[i]);
	}
	return width;
}


int GameActivity::getRemainingCivs()
{
	return startingCivilians-savedCivilians;
}

//Restarts player to level 1
void GameActivity::restart()
{
	//reseting variables on restart
	bullets.clear();
	enemyBullets.clear();
	civ.clear();
	mines.clear();
	enemy.clear();
	blood.clear();
	health.clear();
	wall.clear();

	if (gameDifficulty == 2){
		player1 = new Player(1, 100, 0);
	}
	else if(gameDifficulty == 1){
		player1 = new Player(2, 100, 0);
	}
	else{
		player1 = new Player(3, 100, 0);
	}
	deathTime = -1;
	time = 0;
	level = 1;
	savedCivilians = 0;
	startingCivilians = 0;
	levelCompleteScore = 0;
	levelNeedsChanging = false;
	civilianKilled = false;
	civilianInRange = false; 
	disabledKeys = false;	
	playerDied = false;
	lifeLost = false;
	enemyReset = false;
	camX = camY = 0.0;
	playerX = 5.0;
	playerY = -180.0;
	playerAngle = 0.0;
	turretAngle = 0.0;
	civRenderTimer = 0.0;
	objectiveTimer = 0.0;


	for (int i = 0; i < mapWidth ; i++){
		for (int k = 0; k < mapHeight; k++){

			if (map[k][i] == 1){
				wall.push_back(new Wall (i*4, -k*4, treeWallTextureID));
			}
			if (map[k][i] == 2){
				mines.push_back(new Mine(i*4, -k*4));
			}
			if (map[k][i] == 3){
				wall.push_back(new Wall (i*4, -k*4, wallTextureID));
			}
			if (map[k][i] == 4){
				civ.push_back(new Civilian (i*4, -k*4));
				startingCivilians++;
			}
			if(map[k][i] == 5){
				enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
			}
			if (map[k][i] == 6){
				health.push_back(new Health (i*4, -k*4));
			}

		}
	}
}

void GameActivity::youDied()
{	
	//Reseting the player back to the start on death
	if(level == 1){
		player1->setScore(0);
	}
	else
	{
		player1->setScore(levelCompleteScore);
	}
	bullets.clear();
	enemyBullets.clear();
	health.clear();
	savedCivilians = 0;
	startingCivilians = 0;
	civilianKilled = false;
	civilianInRange = false; 
	disabledKeys = false;	
	playerDied = false;
	enemyReset = false;
	if (level == 1){
		camX = camY = 0.0;
		playerX = 5.0;
		playerY = -180.0;
		playerAngle = 0.0;
		turretAngle = 0.0;
	}
	else if (level == 2){
		playerX = 300.0;
		playerY = -180.0;
		camX = playerX; camY = playerY;
		playerAngle = 0.0;
		turretAngle = 0.0;
	}
	civRenderTimer = 0.0;
	objectiveTimer = 0.0;

	mines.clear();
	civ.clear();
	enemy.clear();
	blood.clear();
	health.clear();

	if (level == 1)
	{
		for (int i = 0; i < mapWidth ; i++){
			for (int k = 0; k < mapHeight; k++){

				if (map[k][i] == 2){
					mines.push_back(new Mine(i*4, -k*4));
				}
				if (map[k][i] == 4){
					civ.push_back(new Civilian (i*4, -k*4));
					startingCivilians++;
				}
				if(map[k][i] == 5){
					enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
				}
				if (map[k][i] == 6){
					health.push_back(new Health (i*4, -k*4));
				}
			}
		}
	}
	else if(level == 2)
	{
		for (int i = 0; i < mapWidth ; i++){
			for (int k = 0; k < mapHeight; k++){

				if (map2[k][i] == 2){
					mines.push_back(new Mine(i*4, -k*4));
				}
				if (map2[k][i] == 4){
					civ.push_back(new Civilian (i*4, -k*4));
					startingCivilians++;
				}
				if(map2[k][i] == 5){
					enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
				}
				if (map2[k][i] == 6){
					health.push_back(new Health (i*4, -k*4));
				}
			}
		}
	}
}

void GameActivity::setDifficulty(int difficulty)
{
	//this is called in DifficultyScreenActivity creating the player object
	if (difficulty == 2){
		//Hard
		player1 = new Player(1, 100, 0);
		mineDamage = 100;
		enemyDamage = 25;
	}
	else if(difficulty == 1){
		//Medium
		player1 = new Player(2, 100, 0);
		mineDamage = 70;
		enemyDamage = 15;
	}
	else{
		//Easy
		player1 = new Player(3, 100, 0);
		mineDamage = 50;
		enemyDamage = 10;
	}

	gameDifficulty = difficulty;
}

void GameActivity::onEnemyHit(int i)
{
	//setting new enemy health on bullet collision
	enemy.at(i)->setHealth(enemy.at(i)->getHealth()-20);
	if(enemy.at(i)->getHealth() <= 0)
	{
		enemy.erase(enemy.begin()+i);
		player1->setScore(player1->getScore()+50);
	}
}

void GameActivity::enemyAI()
{
	//Enemy moves from left to right 
	for (int i  =0; i < enemy.size(); i++){
		double ENEMY_MOVEMENT_SPEED = 1000;
		Enemy* e = enemy.at(i);

		if (e->getEnemyAngle() == 90){
			float pCos = cos(DEG_TO_RAD(-enemyAngle));
			double enemyX = e->getX();
			e->setX(enemyX -= ENEMY_MOVEMENT_SPEED * pCos);	
			if (e->shouldEnemyTurn() == true)
			{
				e->setAngle(270.0);
				e->setTurnTimer(0.0);
			}
		}
		else
		{ 
			float pCos = cos(DEG_TO_RAD(-enemyAngle));
			double enemyX = e->getX();
			e->setX(enemyX += ENEMY_MOVEMENT_SPEED * pCos);	
			if (e->shouldEnemyTurn() == true)
			{
				e->setAngle(90.0);
				e->setTurnTimer(0.0);
			}
		}
	}
}
void GameActivity::isInEnemyDistance()
{
	//If player is within a certain distance of an enemy, they will aim their turret at the player and shoot 
	for (int i = 0; i<enemy.size(); i++){
		Enemy* e = enemy.at(i);
		float dx = playerX - e->getX(), dy = playerY - e->getY();
		if (dx >= -20 && dx < 20)
		{
			if (dy > -20 && dy < 20)
			{
				double angle = RAD_TO_DEG(atan2(dy, dx)) - 90.0;
				e->setTurretAngle(angle);
				if (e->shouldEnemyFire() == true)
				{
					PlaySound("shoot.WAV", NULL, SND_ASYNC);
					enemyBullets.push_back(new bullet(e->getX(), e->getY(), angle));
					e->setTimer(0.0);
				}
			}
		}
	}
}

void GameActivity::civilianAnimation(double deltaT, double prevDeltaT)
{
	//Every second the civilian render image will change to show the civilians moving their arms
	if (civRenderTimer >= 1)
	{
		for (int i = 0; i<civ.size(); i++){
			Civilian* c = civ.at(i);
			c->update(deltaT, prevDeltaT);
			civRenderTimer = 0;
		}
	}
}
void GameActivity::updateBulletLocation(double deltaT, double prevDeltaT)
{
	//Bullet location will be updated every frame
	for (int i = 0; i < bullets.size(); i++){
		bullets.at(i)->update(deltaT, prevDeltaT);
	}
	for (int i = 0; i < enemyBullets.size(); i++){
		enemyBullets.at(i)->update(deltaT, prevDeltaT);
	}
}
void GameActivity::updateEnemyLocation(double deltaT, double prevDeltaT)
{
	//Enemy location will be updated every frame
	for (int i = 0; i < enemy.size(); i++){
		enemy.at(i)->update(deltaT, prevDeltaT);
	}
}

void GameActivity::allCiviliansFound(double deltaT)
{
	if (getRemainingCivs() <= 0){
		objectiveTimer += deltaT;
		//After 3 seconds objective text will disappear and reinforcements will appear
		if (objectiveTimer > 3){
			//Setting the reinforcements once
			if (!enemyReset){
				for (int i = 0; i < mapWidth ; i++){
					for (int k = 0; k < mapHeight; k++){
						if (level == 1)
						{
							if(map[k][i] == 7){
								enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
							}
						}
						else if(level == 2)
						{
							if(map2[k][i] == 7){
								enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
							}
						}
					}
				}
				enemyReset = true;
			}
		}
		if (level == 1){
			//If player gets back to base with all civilians, level complete
			if (playerX < 24 && playerX > 0){
				if(playerY < -168){
					levelNeedsChanging = true;
				}
			}
		}
		//Changes base location for level 2
		else if (level == 2)
		{
			if (playerX < 320 && playerX > 294){
				if(playerY < -168){
					levelNeedsChanging = true;
				}
			}
		}
	}
}

void GameActivity::levelChange()
{
	if (levelChanged)
	{
		if (level == 1)
		{
			level++;
			levelNeedsChanging = false;	
			player1->setHealth(100);
			levelCompleteScore += player1->getScore();

			bullets.clear();
			enemyBullets.clear();
			civ.clear();
			mines.clear();
			enemy.clear();
			blood.clear();
			health.clear();
			wall.clear();

			deathTime = -1;
			time = 0;
			savedCivilians = 0;
			startingCivilians = 0;
			civilianKilled = false;
			civilianInRange = false; 
			disabledKeys = false;	
			playerDied = false;
			lifeLost = false;
			enemyReset = false;
			playerX = 300.0;
			playerY = -180.0;
			camX = playerX; camY = playerY;
			playerAngle = 0.0;
			turretAngle = 0.0;
			civRenderTimer = 0.0;
			objectiveTimer = 0.0;

			for (int i = 0; i < mapWidth ; i++){
				for (int k = 0; k < mapHeight; k++){

					if (map2 [k][i] == 1)
					{
						wall.push_back(new Wall (i*4, -k*4, sandbagTextureID));
					}
					if (map2 [k][i] == 3)
					{
						wall.push_back(new Wall (i*4, -k*4, wallTextureID));
					}

					if (map2[k][i] == 2){
						mines.push_back(new Mine(i*4, -k*4));
					}
					if (map2[k][i] == 4){
						civ.push_back(new Civilian (i*4, -k*4));
						startingCivilians++;
					}
					if(map2[k][i] == 5){
						enemy.push_back(new Enemy (i*4, -k*4, 100,  enemyTurretAngle, enemyAngle));
					}
					if (map2[k][i] == 6){
						health.push_back(new Health (i*4, -k*4));
					}
				}
			}
		}
		//IMPLEMENT LEVEL 3 HERE
	}
	levelChanged = false;
}